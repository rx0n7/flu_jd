import 'dart:ui';

import 'package:flutter/material.dart';

class ScreenLogin extends StatefulWidget {
  @override
  _ScreenLoginState createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  bool _register = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 32),
                child: Image.asset(
                  'images/logo.png',
                  width: 200,
                  height: 150,
                ),
              ),
              TextField(
                autofocus: true,
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(fontSize: 20),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(32, 16, 32, 16),
                    hintText: 'E-mail',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6))),
              ),
              TextField(
                obscureText: true,
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(fontSize: 20),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(32, 16, 32, 16),
                    hintText: 'Senha',
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6))),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Logar'),
                  Switch(
                      value: _register,
                      onChanged: (bool valor) {
                        setState(() {
                          _register = valor;
                        });
                      }),
                  Text('Cadastrar')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

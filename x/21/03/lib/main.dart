import 'package:flutter/material.dart';
import 'package:z_est/src/ScreenLogin.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Pecas Sk8 e Longs',
    home: ScreenLogin(),
  ));
}

import 'package:flutter/material.dart';

//todo
/*
01 - Para fechar a janela quando clicar em 'cancelar' usamos uma funcao 
anonima  com  um 'pop' passando o  'context'(sem chaves)
onPressed: () => Navigator.pop(context),

02 - Fazer o mesmo com 'salvar' mas criar uma funcao salvar dentro do 'onPress'
onPressed: () {
Navigator.pop(context);
},
*/


class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  List _todoList = ['farpar', 'jogar games', 'assistir animes'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
          backgroundColor: Colors.purple,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.purple,
            onPressed: () {
              //
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Adcionar tarefa'),
                      content: TextField(
                        decoration:
                            InputDecoration(labelText: 'Digite sua tarefa'),
                        onChanged: (text) {
                          //
                        },
                      ),
                      actions: [
                        //
                        FlatButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text('Cancelar')),
                        FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('Salvar'))
                      ],
                    );
                  });
            }),
        body: Column(
          children: [
            Expanded(
                child: ListView.builder(
              itemCount: _todoList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  //
                  title: Text(_todoList[index]),
                );
                //
              },
            ))
          ],
        ));
  }
}

import 'package:flutter/material.dart';

class TodoScreen extends StatefulWidget {

//01 - Dentro da coluna retirar o 'texto' e criar um 'Expanded' 
//e colocar como 'filho' uma 'listView; 
////children: [Expanded( child: ListView.builder)
/*02 - Utilizar o 'itemCount', criar uma lista e configurar tres valores, depois 
add no itemCount,configurando com  o tamanho dos itens dentro da lista
itemCount:
List _todoList = ['farpar', 'jogar games', 'assistir animes'];
itemCount: _todoList.length */
//03 - Criar uma funcao anonima no 'itemBuider' que leva um 'context' e um 'index' 
//para fazer a exibicao de cada um dos itens 
////itemBuilder: (context, index) 

//04 - retornar um 'listTile' com 'titulo', que eh a 'lista' de tarefas com o seu 'index'
////return ListTile(title: Text(_todoList[index]),
 

  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  List _todoList = ['farpar', 'jogar games', 'assistir animes'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
          backgroundColor: Colors.purple,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.purple,
            onPressed: () {
              //
            }),
        body: Column(
          children: [
            Expanded(
                child: ListView.builder(
              itemCount: _todoList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  //
                  title: Text(_todoList[index]),
                );
                //
              },
            ))
          ],
        ));
  }
}

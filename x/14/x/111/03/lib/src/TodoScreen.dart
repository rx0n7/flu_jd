import 'package:flutter/material.dart';

//todo

/*01 - Dentro do 'onPress' usar 'showDIalog' que passamos um 'context' e 
ainda dentro do 'dialog' utilizamos o  'buider' que recebe uma funcao anonima 
que tem como parametro um 'context', e dentro do 'buider' retornar o 'alert 
dialog', que recebe um titulo 'adcionar tarefas'
showDialog(context: context,builder: (context) 
return AlertDialog(return AlertDialog(title: 
Text('Adcionar tarefa'), 
*/

//03 - Configurar o conteudo, e usando um 'textFild", e mudar a decoracao dele 
//e add um 'label' com 'digite a sua tarefa'
//content: TextField(decoration:InputDecoration(labelText: 'Digite sua tarefa'),

//04 - Adcionar um 'onChanged' com  uma funcao anonima, que recebe o texto selecionado,
//definimos por enquanto como  'text' mas depois vamos capturar o texto que foi digitado
//onChanged: (text) 

//05 - Apos o 'textFild' configurar as acoes que eh uma 'lista'de 'widget', 
//add um 'botao flat' com um texto como filho chamado 'cancelar'
//FlatButton(child: Text('Cancelar')),

//06 - Definir no 'onPress' uma funcao anonima e duplicar o 'flatButton' e
// colocar 'salvar'
//onPressed: () {}  // child: Text('Salvar'))
                                           
                   

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  List _todoList = ['farpar', 'jogar games', 'assistir animes'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
          backgroundColor: Colors.purple,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.purple,
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Adcionar tarefa'),
                      content: TextField(
                        decoration:
                            InputDecoration(labelText: 'Digite sua tarefa'),
                        onChanged: (text) {
                          //
                        },
                      ),
                      actions: [
                        //
                        FlatButton(
                            onPressed: () {
                              //
                            },
                            child: Text('Cancelar')),
                        FlatButton(
                            onPressed: () {
                              //
                            },
                            child: Text('Salvar'))
                      ],
                    );
                  });
            }),
        body: Column(
          children: [
            Expanded(
                child: ListView.builder(
              itemCount: _todoList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  //
                  title: Text(_todoList[index]),
                );
                //
              },
            ))
          ],
        ));
  }
}

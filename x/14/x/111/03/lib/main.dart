import 'package:flutter/material.dart';

import 'src/TodoScreen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: TodoScreen(),
  ));
}

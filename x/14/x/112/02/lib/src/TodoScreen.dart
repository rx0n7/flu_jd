import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

//todo


//01 - Depois de importar o 'dart.io' retornar 'file' e a string 'path'
//return File();
//02 - Definir o caminho, que jah temos em 'diretorio.path' em formato 
//'string', e colocar o nome do arquivo que queremos salvar
//('${directory.path}'); /// dados.json');
//03 - Usar uma variavel chamada 'archive', e a partir desse arquivo podemos
//usar o '.' ponto e salvar com o metodo de escrever como 'string' e com o 
//conteudo, que vai ser a lista de tarefa que criamos
//var archive =
//archieve.writeAsStringSync(data);
//04 - Antes de salvar os dados no arquivo vamo usar o 'encode' passando  a lista
//json.encode(_todoList);
//05 - Para salvar os dados vamos criar um 'map' de strings 
//Map<String, dynamic> todo = Map();
//06  - Em 'tarefas' criamos um indice chamado 'titulo'  que recebe uma tarefa 'ir ao mercado'
//task['titulo'] = 'Ir ao mercado';
//07 - Em 'tarefa' temos o 'realizada' que eh um boleano
//task['realizada'] = false;
//08 - Adcionando na lista de tarefas essa tarefa que criamos e converte-la para um 'json'
//    _todoList.add(task); // String data = json.encode(_todoList);
//09 - Escrever apenas commo uma string 'sem o sync'
//archieve.writeAsString(data);


class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  //

  List _todoList = [];

  _saveArchive() async {
    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');

    Map<String, dynamic> task = Map();
    task['titulo'] = 'Ir ao mercado';
    task['realizada'] = false;
    _todoList.add(task);

    task['titulo'] = 'Estudar';
    task['realizada'] = false;
    _todoList.add(task);

    String data = json.encode(_todoList);

    archieve.writeAsString(data);
  }

  @override
  Widget build(BuildContext context) {
    //

    _saveArchive();

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de tarefas'),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Adcionar tarefas'),
                    content: TextField(
                      decoration:
                          InputDecoration(labelText: 'Digite uma tarefa'),
                      onChanged: (text) {
                        //
                      },
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text('Salvar')),
                      FlatButton(
                          onPressed: () {
                            //
                            Navigator.pop(context);
                          },
                          child: Text('Cancelar'))
                    ],
                  );
                });
          }),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _todoList.length,
                itemBuilder: (context, index) {
                  //

                  return ListTile(
                    title: Text(_todoList[index]),
                  );
                }),
          )
        ],
      ),
    );
  }
}

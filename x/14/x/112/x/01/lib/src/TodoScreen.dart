import 'package:flutter/material.dart';

import 'package:path_provider/path_provider.dart';\


//01 - Add a depencia do 'path_provider' e importar pra usar os recursos aqui
//path_provider: ^1.6.27
//import 'package:path_provider/path_provider.dart';

//02 - Criar um metodo para salvar os dados e deixar esse metodo assincronomo
//_saveArchive() {}
//sync

//03 - Informando o local que queremos salvar o arquivo, usamos um 'final' mais
//o nome da variavel e o 'getDiretory' e transformar ele em assincrono
//final directory = getApplicationDocumentsDirectory();
//await

//04 - Imprimindo onde esta salvando o arquivo
// print('Caminho: ' + directory.path);

//Caminho: /data/user/0/com.example.z_est/app_flutter

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  //

  List _todoList = ['Ir comprar pao', 'pegar coco', 'ir na a'];

  _saveArchive() async {
    //
    final directory = await getApplicationDocumentsDirectory();
    print('Caminho: ' + directory.path);
  }

  @override
  Widget build(BuildContext context) {
    //

    _saveArchive();

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de tarefas'),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Adcionar tarefas'),
                    content: TextField(
                      decoration:
                          InputDecoration(labelText: 'Digite uma tarefa'),
                      onChanged: (text) {
                        //
                      },
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text('Salvar')),
                      FlatButton(
                          onPressed: () {
                            //
                            Navigator.pop(context);
                          },
                          child: Text('Cancelar'))
                    ],
                  );
                });
          }),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _todoList.length,
                itemBuilder: (context, index) {
                  //

                  return ListTile(
                    title: Text(_todoList[index]),
                  );
                }),
          )
        ],
      ),
    );
  }
}

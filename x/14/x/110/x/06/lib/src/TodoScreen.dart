import 'package:flutter/material.dart';

//todo
/*01 - Cor de primeiro plano(dentro do botao)
       foregroundColor: Colors.red,
       foregroundColor: Colors.white,
*/
  


class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),

        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          child: Icon(Icons.add),
          onPressed: () {
            print('Resultado: Botao pressionado');
          },
        ),
      );
  }
}

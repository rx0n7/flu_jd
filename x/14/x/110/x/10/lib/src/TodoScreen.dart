import 'package:flutter/material.dart';

//todo

/*01 - Criar um botom votao de navegar(em baixo do botao flutuante) com um botao bar, uma linha como filho e um icone de botao como filhos, que recebe um icon, Icon, Icons.menu e um onPressed
bottomNavigationBar: BottomAppBar(child: Row(children:
[IconButton(icon: Icon(Icons.menu), onPressed: () {},

02 - Opcional: Mudar icone add
icon: Icon(Icons.add),
*/
 

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),

         floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          child: Icon(Icons.add),
          onPressed: () {
            print('Resultado: Botao pressionado');
          },
        ),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            children: [
              IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
              ),
            ],
          ),
        ));
  }
}

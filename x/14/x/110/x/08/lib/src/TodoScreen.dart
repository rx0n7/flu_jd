import 'package:flutter/material.dart';

//todo

//01 - Tamanho do botao
//mini: true,

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),

        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          mini: true,
          child: Icon(Icons.add),
          onPressed: () {
            print('Resultado: Botao pressionado');
          },
        ),
      );
  }
}

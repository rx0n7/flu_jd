import 'package:flutter/material.dart';

//todo
//01 - Usar o 'extend' para exibir um icone diferente(no botao flutuante)
//loatingActionButton.extended

//02 - Adcionar um icone de shoping car no botao
//icon: Icon(Icons.add_shopping_cart),

//03 - Criar uma lavel com nome 'adcionar'
//label: Text('Adcionar'),


class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          icon: Icon(Icons.add_shopping_cart),
          label: Text('Adcionar'),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            children: [
              IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
              ),
            ],
          ),
        ));
  }
}

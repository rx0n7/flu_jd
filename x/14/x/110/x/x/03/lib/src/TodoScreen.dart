import 'package:flutter/material.dart';

//todo
//01 - Criar um 'print' que exiba 'botao pressioando'no console o que foi acionado
//// onPressed: () {print('');},

//Botao pressionado

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Floating action button'),
      ),
      body: Text('Conteudo'),
      floatingActionButton: FloatingActionButton(
        child: Text('teste'),
        onPressed: () {
          print('Resultado: Botao pressionado');
        },
      ),
    );
  }
}

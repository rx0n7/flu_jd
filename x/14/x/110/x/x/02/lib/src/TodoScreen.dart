import 'package:flutter/material.dart';

//todo
//01 - Escrever um botao flutuante abaixo do 'texto' conteudo com um 'onPress'
//e  uma funcao anonima e um 'print' com o texto 'botao pressionado'
////floatingActionButton: FloatingActionButton //onPressed:() {print('');

//02 -Definindo o 'icone' no 'botao flutuante' com o texto 'teste' abaixo do 'botao'
////child: Text('teste')
       

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Floating action button'),
      ),
      body: Text('Conteudo'),
      floatingActionButton: FloatingActionButton(
        child: Text('teste'),
        onPressed: () {
          print('Resultado: Botao pressionado');
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';

//todo
//01 - Definir um appbar, um titulo 'Botao Flutuante', e corpo com um texto 'conteudo'
////title: Text('Floating action button'), //body: Text('Conteudo'),

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Floating action button'),
      ),
      body: Text('Conteudo'),
    );
  }
}

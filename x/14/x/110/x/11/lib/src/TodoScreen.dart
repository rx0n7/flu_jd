import 'package:flutter/material.dart';

//todo

//01 - Deixaro o floatingBottom junto com o bottomAppBar e centraliado
//endDocked, centerDocked,

//02 -  Fazer a integracao entre os botoes(embaixo botao navegador)
//shape: CircularNotchedRectangle(),

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),

        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

         floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          child: Icon(Icons.add),
          onPressed: () {
            print('Resultado: Botao pressionado');
          },
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          child: Row(
            children: [
              IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
              ),
            ],
          ),
        ));
  }
}

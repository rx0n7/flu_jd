import 'package:flutter/material.dart';

//todo

/*01 - Posicionamento do botao flutuante em varios lugares da tela(add acima botao)
FloatingActionButtonLocation: centerFloat, startTop, endTop, endFloat 
*/



class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de Tarefas'),
        ),
        body: Text('Conteudo'),
         floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
         floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          child: Icon(Icons.add),
          onPressed: () {
            print('Resultado: Botao pressionado');
          },
        ),
        );
  }
}

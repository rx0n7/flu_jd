import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

//todo

//01 - Caso o valor seja nulo exibindo uma mensagem 
// ('nome') ?? 'Sem valor';
// 02 - Para remover usamos a mesma instancia do 'prefs', e colocar remove em modo assyncrono
//final prefs = await SharedPreferences.getInstance();
//_remove() async {
//03 - Utilizar o 'prefs.remove' passando a chave que queremos remover e fazer a exibicao
//prefs.remove('nome');
//print('Operacao (remover)');

//Operacao (recuperar): Sem valo

class DadosScreen extends StatefulWidget {
  @override
  _DadosScreenState createState() => _DadosScreenState();
}

class _DadosScreenState extends State<DadosScreen> {
  String _textSaved = 'Nada salvo';

  TextEditingController _controllerField = TextEditingController();

  _save() async {
    //
    String valueTyped = _controllerField.text;

    final prefs = await SharedPreferences.getInstance();
    prefs.setString('nome', valueTyped);

    print('Operacao (salvar): $valueTyped');
  }

  _recovery() async {
    final prefs = await SharedPreferences.getInstance();

    setState(() {
      _textSaved = prefs.getString('nome') ?? 'Sem valor';
    });

    print('Operacao (recuperar): $_textSaved');
  }

  _remove() async {
    final prefs = await SharedPreferences.getInstance();
    
    prefs.remove('nome');

    print('Operacao (remover)');

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manipulação de dados"),
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            Text(
              _textSaved,
              style: TextStyle(fontSize: 20),
            ),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Digite algo"),
              controller: _controllerField,
            ),
            Row(
              children: <Widget>[
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Salvar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _save(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Recuperar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _recovery(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Apagar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _remove(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

//todo

//01 - Para recuperar  usando o mesmo metodo e transformar o metodo
// recuperar em assincrono
// final prefs = await SharedPreferences.getInstance();
//_recovery() async
//02 - Recuperar a string com a chave que passamos acima 'nome'
//prefs.getString('nome');

//03 - Utilizar um 'setState' para configurar o 'textoSalvo' e colocar o
//'textSaved' no lugar do texto(se ainda nao colocou
//setState(() {
//_textSaved = prefs.getString('nome');
//_textSaved,

//04 - Da um 'print' em 'recuperar' e exibir o texto salvo
//print('Operacao (recuperar): $_textSaved');

//Operacao (recuperar): ee

class DadosScreen extends StatefulWidget {
  @override
  _DadosScreenState createState() => _DadosScreenState();
}

class _DadosScreenState extends State<DadosScreen> {
  String _textSaved = 'Nada salvo';

  TextEditingController _controllerField = TextEditingController();

  _save() async {
    String valueTyped = _controllerField.text;

    final prefs = await SharedPreferences.getInstance();
    prefs.setString('nome', valueTyped);

    print('Operacao (salvar): $valueTyped');
  }

  _recovery() async {
    final prefs = await SharedPreferences.getInstance();

    setState(() {
      _textSaved = prefs.getString('nome');
    });

    print('Operacao (recuperar): $_textSaved');
  }

  _remove() {
    //
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manipulação de dados"),
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            Text(
              _textSaved,
              style: TextStyle(fontSize: 20),
            ),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Digite algo"),
              controller: _controllerField,
            ),
            Row(
              children: <Widget>[
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Salvar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _save(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Recuperar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _recovery(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Apagar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _remove(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

//todo

//01 - criar uma 'string' com o texto 'nada salvo', e apagar o 'nada-salvo'
//em 'text' e colocar o texto que criamos
////String textSaved = 'Nada salvo'; //Text(_textSaved)
//02 - Dentro do metodo 'salvar' definir uma 'string' chamada 'valor-//digitado,
//e recuperar o que foi digitado
////String valueTyped = _controllerField.text;
//03 - Pra salvar o valor digitado na preferencia do usuario vamos importar o 'plugin'
////shared_preferences: ^0.5.12+4
//04 - Criando um 'final' e chamando de 'prefs' que recebe um 'getInstance' para
//manipular os dados do usuario
////final prefs = SharedPreferences.getInstance();
//05 - Transofrmar o metodo salvar em assimcrono e utilizar o 'await'
//_save() async
//06 - Usando o 'prefs' mais ponto '.' aparece o 'setString' com uma 'chave'
//e o 'valor', que vai ser o valor digitado
//prefs.setString('nome' + valueTyped);
//07 - Transformar em assincrono pois pode demorar pra ser digitado
//await
//08 - Da um 'print' pra exibir o que foi salvor
//print('Operacao (salvar): $valueTyped');

//Operacao (salvar): teste


class DadosScreen extends StatefulWidget {
  @override
  _DadosScreenState createState() => _DadosScreenState();
}

class _DadosScreenState extends State<DadosScreen> {
  String _textSaved = 'Nada salvo';

  TextEditingController _controllerField = TextEditingController();

  _save() async {
    String valueTyped = _controllerField.text;

    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('nome', valueTyped);

    print('Operacao (salvar): $valueTyped');
  }

  _recovery() {
    //
  }

  _remove() {
    //
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manipulação de dados"),
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            Text(
              _textSaved,
              style: TextStyle(fontSize: 20),
            ),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Digite algo"),
              controller: _controllerField,
            ),
            Row(
              children: <Widget>[
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Salvar", style: TextStyle(fontSize: 20)),
                  onPressed: () => _save(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Recuperar", style: TextStyle(fontSize: 20)),
                  onPressed: _recovery(),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  child: Text("Apagar", style: TextStyle(fontSize: 20)),
                  onPressed: _remove(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:z_est/src/DadosScreen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: DadosScreen(),
  ));
}

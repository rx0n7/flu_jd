import 'package:flutter/material.dart';

//todo
//01 - Titulo do app, padding medio, e uma coluna com filhos
////'manipulacao de dados'/ 32 / column /children

//02 - Add texto com a frase 'nada salvo' e Formatar com uma fonte pequena/media
////Text("Nada salvo!",style: TextStyle(fontSize: 20),),

//03 - Add campo de texto, com entrada do tipo 'texto' e um 'label' com 'digite algo'
/////TextField(keyboardType: TextInputType.text,decoration: InputDecoration(labelText: "Digite algo"),

//04 - Definir o controler para controlar o campo de nome 'controllerField'
////TextEditingController _controllerCampo = TextEditingController();

//05 - Add o controller que criamos acima no controller do botao
////controller: _controllerCampo

//06 - Apos o 'textFild' criar uma 'linha' como 'filho' e um 'botao' de cor azul,
////RaisedButton(color: Colors.blue

//A cor do texto 'branco' e o 'espacamento' de todos os lados pequeno
////textColor: Colors.white, //padding(15),

//E como 'filho' colocar um texto com nome 'salvar' e o tamanho medio
//// fonte (20_)

//07 - Criar os metodos 'salvar' e add no 'onPressed', depois duplicar os
//'raiseButons' e criar os metodos 'recuperar' e 'apagar' e add nos 'onPressed
////_save() {}
////_recoveryData() {}
////_deleteData() {}

class DadosScreen extends StatefulWidget {
  @override
  _DadosScreenState createState() => _DadosScreenState();
}

class _DadosScreenState extends State<DadosScreen> {
  //
  _saveData() {
    //
  }

  _recoveryData() {
    //
  }

  _deleteData() {
    //
  }

  TextEditingController _controllerField = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Maninupacao de dados'),
        backgroundColor: Colors.red,
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: [
            Text('Nada Salvo',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: 'Digite algo: '),
              controller: _controllerField,
            ),
            Row(
              children: [
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  onPressed: _saveData(),
                  child: Text('Salvar'),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  onPressed: _recoveryData(),
                  child: Text('Recuperar'),
                ),
                RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(15),
                  onPressed: _deleteData(),
                  child: Text('Apagar'),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

//todo

/*
try {
      
    } catch (e) {
    }

      return null;

      final archive = await _getFile();

    var archive = await _getFile();

    archive.readAsString();

void initState() {
    super.initState();
  }

      archive.readAsString();

     return archive.readAsString();

    _readArchive().then((data) {

      setState(() {

        _todoList = json.decode(data);


*/

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  //

  List _todoList = [];

  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory();

    return File('${directory.path}/dados.json');
  }

  _saveArchive() async {
    var archive = await _getFile();
    archive.readAsString();

    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');

    Map<String, dynamic> task = Map();
    task['titulo'] = 'Ir ao mercado';
    task['realizada'] = false;
    _todoList.add(task);

    task['titulo'] = 'Estudar';
    task['realizada'] = false;
    _todoList.add(task);

    String data = json.encode(_todoList);

    archieve.writeAsString(data);
  }

  _readArchive() async {
    //

    try {
      final archive = await _getFile();

      return archive.readAsString();
    } catch (e) {
      return null;
    }
  }

  @override
  void initState() {
    super.initState();

    _readArchive().then((data) {
      setState(() {
        _todoList = json.decode(data);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //

    //_saveArchive();
    print('Itens: ' + _todoList.toString());

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de tarefas'),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Adcionar tarefas'),
                    content: TextField(
                      decoration:
                          InputDecoration(labelText: 'Digite uma tarefa'),
                      onChanged: (text) {
                        //
                      },
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text('Salvar')),
                      FlatButton(
                          onPressed: () {
                            //
                            Navigator.pop(context);
                          },
                          child: Text('Cancelar'))
                    ],
                  );
                });
          }),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _todoList.length,
                itemBuilder: (context, index) {
                  //

                  return ListTile(
                    title: Text(_todoList[index]),
                  );
                }),
          )
        ],
      ),
    );
  }
}

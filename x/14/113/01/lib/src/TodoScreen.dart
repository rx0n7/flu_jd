import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

//todo

//01 - 
//*_readArchive() {

//_readArchive() async {
    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');

  _getFile() {


  _getFile() async {
    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');


    return File('${directory.path}/dados.json');

    var archive = _getFile();

    var archive = await _getFile();
    */

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  //

  List _todoList = [];

  _getFile() async {
    final directory = await getApplicationDocumentsDirectory();

    return File('${directory.path}/dados.json');
  }

  _saveArchive() async {
    var archive = await _getFile();

    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');

    Map<String, dynamic> task = Map();
    task['titulo'] = 'Ir ao mercado';
    task['realizada'] = false;
    _todoList.add(task);

    task['titulo'] = 'Estudar';
    task['realizada'] = false;
    _todoList.add(task);

    String data = json.encode(_todoList);

    archieve.writeAsString(data);
  }

  _readArchive() async {
    final directory = await getApplicationDocumentsDirectory();

    var archieve = File('${directory.path}/dados.json');
  }

  @override
  Widget build(BuildContext context) {
    //

    _saveArchive();

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de tarefas'),
        backgroundColor: Colors.purple,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.purple,
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Adcionar tarefas'),
                    content: TextField(
                      decoration:
                          InputDecoration(labelText: 'Digite uma tarefa'),
                      onChanged: (text) {
                        //
                      },
                    ),
                    actions: [
                      FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text('Salvar')),
                      FlatButton(
                          onPressed: () {
                            //
                            Navigator.pop(context);
                          },
                          child: Text('Cancelar'))
                    ],
                  );
                });
          }),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: _todoList.length,
                itemBuilder: (context, index) {
                  //

                  return ListTile(
                    title: Text(_todoList[index]),
                  );
                }),
          )
        ],
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

//todo
/*
https://pub.dev/packages/google_maps_flutter

    <meta-data android:name="com.google.android.geo.API_KEY"
               android:value="AIzaSyDLUlSqRicS73mmCe-4dyvV5e0hYSPfJzY"/
//AIzaSyDLUlSqRicS73mmCe-4dyvV5e0hYSPfJzY

  google_maps_flutter: ^1.1.1

import 'package:google_maps_flutter/google_maps_flutter.dart';

  Completer<GoogleMapController> _controller = Completer();

appBar: AppBar(
        title: Text('Mapas e geolocalizacao'),
      )
      body: Container(
        child: GoogleMap(initialCameraPosition: null),
 

          mapType: MapType.normal,
CameraPosition(target: )

                CameraPosition(target: LatLng(-12.972842, 38.510900))),

                target: LatLng(-12.972842, 38.510900), zoom: 16), onMapCreated: () {

                target: LatLng(-12.972842, 38.510900), zoom: 16), onMapCreated: (GoogleMapController controller) {

            _controller.complete(controller);
*/

class NearScreen extends StatefulWidget {
  @override
  _NearScreenState createState() => _NearScreenState();
}

class _NearScreenState extends State<NearScreen> {
  //

  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mapas e geolocalizacao'),
      ),
      body: Container(
        child: GoogleMap(
          mapType: MapType.normal,

          //-12.972842, -38.510900
          initialCameraPosition:
              CameraPosition(target: LatLng(-12.972842, 38.510900), zoom: 16),
          onMapCreated: (GoogleMapController controller) {
            //
            _controller.complete(controller);
          },
        ),
      ),
    );
  }
}

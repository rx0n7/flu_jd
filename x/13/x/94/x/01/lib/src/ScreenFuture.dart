import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:z_est/src/Post.dart';

import 'package:http/http.dart' as http;

//todo
//01 - Pegar a 'url' base para ser consumida na aplicacao 
////"https://jsonplaceholder.typicode.com/";
///02 - Criar uma "string" para a "url" base e configura-la
////String _urlBase = "https://jsonplaceholder.typicode.com/";
//03 - Criar um metodo para recupear postagens e no "future" do 'buider' usar ele
////_receiverPosts() //future: _receiverPosts,
//04 - Add um 'appBar' e um titulo, e colar o 'futureBuider' dentro de 'body'
////consumo de servico avancadao
//04 - Para retornar uma "lista" de itens usamos o "Fututure" que retorna uma "lista", e 
// dentro dessa lista criamos uma lista de "objetos" que eh um objeto "POST"
////Future<List<Post>>
/*
//05 - Ir em " Src>Post.dart" e criar uma classe chamada "POst"
////class Post
//06 - Em  seguida definir os atributos com os seus tipos
//// _userId; _id; _title; _body;
//07 - Criar construtores nomeados para a nossa "Classe" para poder acessar os campos acima
/////Post(this._userId, this._id, this._title, this._body);
//08 - EM "home" tentar instanciar a classe "Post" mas vai dar erro pois como estamos 
//usando  o "underline" precisamos configurar os "get and seters"
//String get body => _body; // set body(String value) {_body = value;}
*/
/*09 - Instanciando a classe "Post" na 'Home' e passar os valores valores(int e string)
//Post post = Post();  //Post post = Post(1, 1, "", "");
//10 - Criar uma "lista" vazia e dentro dessa "lista" temos "Post", chamamos ela de "lista" 
// e inicializamos uma lista vazia, com apenas o "List"
////List<Post> lista = List();
// 11 - Agora ao colocar "lista.add" podemos acionar o "Post" que vamos criar
////lista.add();
//12 - Criando "post" e instanciando a  classe
////Post post = Post();
13 - Agora o objeto "post" vai ser adcionado dentro da lista
////lista.add(post);
*/
//14 - No "futureBuider" fazemos a mesma coisa, onde colocamos "Map" colocamos o "List>Post"
////FutureBuilder<List<Post>>
//15 - Criar um "GET" utilizando o "http" e o "Response" com a variavel da "url", 
//concatenando com a postagem
////http.Response response =  http.get(_urlBase + "posts");
//16 - Transformando a requisicao e o metodo de forma assyncrona
////assync / await
//17 - Decodificar o corpo da postagem com um objeto "JSON" dentro de uma variavel
//var dadosJson = json.decode(response.body);
//
//18 - Imprimindo o objeto"JSON"
//print(dadosJson);
//{userId: 1, id: 1, title: sunt aut facere repellat provident quia et suscipit

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {

  String _urlBase = 'https://jsonplaceholder.typicode.com/';

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase + 'posts');
    var dadosJson = json.decode(response.body);

    print(dadosJson);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico avancado'),
      ),
      body: FutureBuilder<List<Post>>(
        future: _receiverPosts(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              print("Waiting");
              break;
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError) {
              } else {
                //
              }
          }
          return Container();
        },
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:z_est/src/Post.dart';

import 'package:http/http.dart' as http;

//todo
//01
//// return dadosJson;
//02 - Percorrer os dados 'JSON" em cada um dos 'posts' e armazenar em "post"
//e retornar uma lista de postagens
// for(var post in dadosJson)
//02 - Acessando cada um dos Posts('post') separados pelo 'indice' e acesando "title"
//   print("post:" + post["title"]);
//03 - Criando um "Post" e chamando de "p" que recebe a instanciando "POST"
//Post p = Post(post["userId"], post["Id"], post["title"], post["body"]);
//04 - Criando uma 'lista' de postagens, que chamamos de 'postagens' que recebe o
//instanciamento de 'List'
//List<Post> postings = List();
//05 - Adcionado as postagens com "add" e agora temos uma lista de postagens
//postings.add(p);
//06 - Fazendo a exibicao
//print(postings.toString());

//Instance of 'Post', Instance of 'Post', Instance of 'Post', Instance of 'Post', Instance of 'Post'

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {
  String _urlBase = 'https://jsonplaceholder.typicode.com/';

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase + 'posts');
    var dadosJson = json.decode(response.body);

    List<Post> postings = List();
    for (var post in dadosJson) {
      print('post' + post['title']);

      Post p = Post(post['userId'], post['id'], post['title'], post['body']);
      postings.add(p);
    }

    print(postings.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico avancado'),
      ),
      body: FutureBuilder<List<Post>>(
        future: _receiverPosts(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              print("Waiting");
              break;
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError) {
              } else {
                //
              }
          }
          return Container();
        },
      ),
    );
  }
}

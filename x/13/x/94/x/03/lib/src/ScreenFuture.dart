import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:z_est/src/Post.dart';

import 'package:http/http.dart' as http;

//todo

//01 - retornar a lista de postagens que montamos utilizando o 'FOR'
////return postagens;

//02 - Colocar um print no "IF" e no "Else" do 'hasError'
//print("Erro ao carregar lista"); //("Lista carregou");

//03 - Exibir uma barra de progresso com um "return" depois de 'waiting'
//return circularProgressIndicator
//04 - Retornar um listVIew.buider no 'Else' diretamente
//return ListView.builder()
//

//05 - Definir um "itemCount" para contar a quantidade de intens
// itemCount: snapshot.data.length,
//06 - Dentro do 'itemBuider' usar duas funcoes anonimas
//(context, index)
//07 - Dentro da lista retornar um 'listTitle' com titulo e subtitulo chamado 'teste'
//return ListTile
//title: Text("testes"), subtitle: Text("testes"));

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {
  String _urlBase = 'https://jsonplaceholder.typicode.com/';

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase + 'posts');
    var dadosJson = json.decode(response.body);

    List<Post> postings = List();
    for (var post in dadosJson) {
      print('post' + post['title']);

      Post p = Post(post['userId'], post['id'], post['title'], post['body']);
      postings.add(p);
    }

    return postings;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico avancado'),
      ),
      body: FutureBuilder<List<Post>>(
        future: _receiverPosts(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              Center(
                child: CircularProgressIndicator(),
              );
              break;
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError) {
                print('Lista nao carregou');
              } else {
                print('Lista carregou');

                return ListView.builder(
                    itemCount: snapshot.data.length,
                    //
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text('teste'),
                        subtitle: Text('teste'),
                      );
                      //
                    });
              }
          }
          return Container();
        },
      ),
    );
  }
}

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:z_est/src/Post.dart';



class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {

  String _urlBase = "https://jsonplaceholder.typicode.com/posts";

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase);
    var dadosJson = json.decode(response.body);

    List<Post> postagens = List();

    for (var post in dadosJson) {
      print("post" + post["title"]);

      Post p =
          Post(post["_userId"], post["_id"], post["_title"], post["_body"]);

      postagens.add(p);
    }
    print(postagens.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Future"),
      ),
      body: FutureBuilder<List<Post>>(
        future: _receiverPosts(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return CircularProgressIndicator();
              break;
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError) {
                print("Erro ao carregar ");
              } else {
                print("Lista Carregou ");

                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                          title: Text("testes"), subtitle: Text("testes"));
                    });
              }
          }
          return Container();
        },
      ),
    );
  }
}

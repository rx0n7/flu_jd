import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

//todo
//01 -
// print('Retorno' + back['BRL'].toString() );
//I/flutter ( 2364): Retorno{15m: 162327.33, last: 162327.33, buy: 162327.33, sell: 162327.33, symbol: R$}

//02 - 
 ////print('Retorno: ' + back['BRL']['buy'].toString() );
Retorno: 162561.09


class SearchZip extends StatefulWidget {
  @override
  _SearchZipState createState() => _SearchZipState();
}

class _SearchZipState extends State<SearchZip> {
  String _priceBitpai = '0';

  void _receiverPrice() async {
    String url = 'https://blockchain.info/ticker';
    http.Response response = await http.get(url);

    Map<String, dynamic> back = json.decode(response.body);

    print('Retorno: ' + back['BRL']['buy'].toString() );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('images/bitcoin.png'),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Text(
                  'R\$' + _priceBitpai,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                  child: Text(
                    'Atualizar',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.orange,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  onPressed: _receiverPrice)
            ],
          ),
        ),
      ),
    );
  }
}

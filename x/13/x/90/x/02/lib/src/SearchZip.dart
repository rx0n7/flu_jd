import 'dart:convert';

import 'package:flutter/material.dart';

//todo
// 01 -
//mainAxisAlignment: MainAxisAlignment.center,

class SearchZip extends StatefulWidget {
  @override
  _SearchZipState createState() => _SearchZipState();
}

class _SearchZipState extends State<SearchZip> {
  String _priceBitpai = '0';

  void _receiverPrice()  {
   //
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('images/bitcoin.png'),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Text(
                  'R\$' + _priceBitpai,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                  child: Text(
                    'Atualizar',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.orange,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  onPressed: _receiverPrice)
            ],
          ),
        ),
      ),
    );
  }
}

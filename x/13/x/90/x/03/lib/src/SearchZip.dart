import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

//todo
//01 - 
//// String url = 'https://blockchain.info/ticker';
// 02 -
//// import 'package:http/http.dart' as http;
//03 -
//// void _receiverPrice() async {
//04 - 
////http.Response response = http.get(url);
//05 - 
//// await http.get(url);
//06 - 
////Map<String, dynamic> 
//07 - 
////back = json.decode(response.body);
//08 - 
////print('Retorno' + back.toString());

//I/flutter ( 2192): Retorno{USD: {15m: 31039.41, last: 31039.41, buy: 31039.41, sell: 31039.41, symbol: $}, AUD: {15m: 40191.38, last: 40191.38, buy: 40191.38, sell: 40191.38, symbol: $}, BRL: {15m: 161212.49, last: 161212.49, buy: 161212.49, sell: 161212.49, symbol: R$}, CAD: {15m: 39381.16, last: 39381.16, buy: 39381.16, sell: 39381.16, symbol: $}, CHF: {15m: 27311.3, last: 27311.3, buy: 27311.3, sell: 27311.3, symbol: CHF}, CLP: {15m: 21851739.08, last: 21851739.08, buy: 21851739.08, sell: 21851739.08, symbol: $}, CNY: {15m: 200613.91, last: 200613.91, buy: 200613.91, sell: 200613.91, symbol: ¥}, DKK: {15m: 187820.25, last: 187820.25, buy: 187820.25, sell: 187820.25, symbol: kr}, EUR: {15m: 25210.79, last: 25210.79, buy: 25210.79, sell: 25210.79, symbol: €}, GBP: {15m: 22706.48, last: 22706.48, buy: 22706.48, sell: 22706.48, symbol: £}, HKD: {15m: 240634.58, last: 240634.58, buy: 240634.58, sell: 240634.58, symbol: $}, INR: {15m: 2265982.15, last: 2265982.15, buy: 2265982.15, sell: 2265982.15, symbol: ₹}, ISK: {15m: 39



class SearchZip extends StatefulWidget {
  @override
  _SearchZipState createState() => _SearchZipState();
}

class _SearchZipState extends State<SearchZip> {
  String _priceBitpai = '0';

  void _receiverPrice() async {
    String url = 'https://blockchain.info/ticker';
    http.Response response = await http.get(url);

    Map<String, dynamic> back = json.decode(response.body);

    print('Retorno' + back.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('images/bitcoin.png'),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Text(
                  'R\$' + _priceBitpai,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                  child: Text(
                    'Atualizar',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.orange,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  onPressed: _receiverPrice)
            ],
          ),
        ),
      ),
    );
  }
}

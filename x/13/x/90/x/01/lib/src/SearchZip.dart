import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

//01 - Importar a biblioteca "http" no "pubspeck" e a imagem do bitcoin
////http: ^0.12.2
////Image.asset('images/bitcoin.png'),
//02 - Criar um 'Container' com um 'Padding' medio e deixar conteudo do 'Container' centralizado
////edge all 32//child: Center
//03 - Adcionar uma 'coluna' como um 'filho' e uma iamgem como 'filhos'
//child: Column( children: [
////child Image.asset("images/bitcoin.png"),
//04 - Colocar um 'padding' entre o 'texto', a imagem e o 'botao' e um texto como 'filho'
//// edge only: top 30 / booton 30
//// child: Text
//05 - Adcionar um 'texto' com o preco em 'reais' pra isso usar 'escape'
// child: Text("R\$")
//06 - Criar uma 'string' com valor inicial '0' e fazer a exibicao do 'preco'
////String _priceBitpai = "0";
////'R\$' + _priceBitpai
//07 - Definir um 'style' com uma fonte grande
////style: TextStyle(fontSize: 35),
//08 - Configurar um botao apos o 'padding' com um texto: 'Atualizar' com uma fonte media 
// de cor branca e negrita
////raisebOtton, child: Text('') // fontSize: 25, FontWeight.bold, Colors.white
//09- Definir a cor 'laranja' para o 'botao' com espacamento de cada um dos lados de '30,15,10,15'
////color: Colors.orange
////padding: EdgeInsets.fromLTRB(30, 15, 30, 15I)
//10 - Criar funcao para 'recuperarPreco' sem retorno e usar no 'onPress' do botao
////void _recoverPrice()
////onPressed: _receiverPrice


class SearchZip extends StatefulWidget {
  @override
  _SearchZipState createState() => _SearchZipState();
}

class _SearchZipState extends State<SearchZip> {
  String _priceBitpai = '0';

  void _receiverPrice() async {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            children: [
              Image.asset('images/bitcoin.png'),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Text(
                  'R\$' + _priceBitpai,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                  child: Text(
                    'Atualizar',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.orange,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  onPressed: _receiverPrice)
            ],
          ),
        ),
      ),
    );
  }
}

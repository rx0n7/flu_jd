import 'dart:ui';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

//todo

//01 - Recuperar apenas o valor do Brasil
//retorno["BRL"]
//02 - Acessar apenas o BUY como string
//["BRL" "buy".ToString]

//O retorno eh: {15m: 54082.47, last: 54082.47, buy: 54082.47, sell: 54082.47, symbol: R$}

class BitPrice extends StatefulWidget {
  @override
  _BitPriceState createState() => _BitPriceState();
}

class _BitPriceState extends State<BitPrice> {
  String _priceBitpai = "0";

  void _recoverPrice() async {
    String url = "https://blockchain.info/ticker";
    http.Response response = await http.get(url);

    Map<String, dynamic> retorno = json.decode(response.body);
    print("O retorno eh: " + retorno["BRL"].toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("App Preco do Bitpai"),
          backgroundColor: Colors.amber,
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.all(32),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("images/bitcoin.png"),
                Padding(
                  padding: EdgeInsets.only(top: 32, bottom: 32),
                  child: Text(
                    _priceBitpai + "R\$",
                    style: TextStyle(fontSize: 25),
                  ),
                ),
                RaisedButton(
                    padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                    color: Colors.orange,
                    child: Text(
                      "Atualizar",
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    onPressed: () {
                      _recoverPrice();
                    })
              ],
            ),
          ),
        ));
  }
}

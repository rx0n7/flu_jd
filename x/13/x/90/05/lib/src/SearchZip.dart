import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

//todo
//    setState(() { _priceBitpai = back['BRL']['buy'].toString(); });
// *Na tele de exibicao
// 157698.53

 

class SearchZip extends StatefulWidget {
  @override
  _SearchZipState createState() => _SearchZipState();
}

class _SearchZipState extends State<SearchZip> {
  String _priceBitpai = '0';

  void _receiverPrice() async {
    String url = 'https://blockchain.info/ticker';
    http.Response response = await http.get(url);

    Map<String, dynamic> back = json.decode(response.body);

    setState(() {
      _priceBitpai = back['BRL']['buy'].toString();
      print(_priceBitpai);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('images/bitcoin.png'),
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Text(
                  'R\$' + _priceBitpai,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                  child: Text(
                    'Atualizar',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.orange,
                  padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                  onPressed: _receiverPrice)
            ],
          ),
        ),
      ),
    );
  }
}

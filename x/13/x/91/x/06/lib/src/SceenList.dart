import 'package:flutter/material.dart';

//todo
//01 - Ao inves de colocar cada item do array para um objeto "map" podemos fazer diretamente, 
// esse trecho vai retornar um objeto do tipo "map", e em seguida conseguimos acessar o "array"
//_itens[indice]["titulo"]),

//02 - Adcionar a "descricao" tambem no "subtitulo"
//_itens[indice]["descricao"]),

class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {

            return ListTile(
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),
            );
          }),
    );
  }
}

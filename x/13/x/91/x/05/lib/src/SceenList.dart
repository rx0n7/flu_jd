import 'package:flutter/material.dart';

//todo
//01 - Para capturar o "map" utilizamos a mesma estrutura que criamos sem instanciar
//// Map<String, dynamic> item = ;

//02 - Colocar cada um do objeto que eh um "map" e colocando de "item" de acordo com o "indice"
////Map<String, dynamic> item = _itens[indice];
//03 - Fazer a exibicao do "item" baseado no "indice"
//print("Item: " + item["titulo"]);

// Item: Titulo: 0 skdjfkldjf



class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            Map<String, dynamic> item = _itens[indice];
            print("Item: " + item["titulo"]);
            return ListTile(
              title: Text(indice.toString()),
              subtitle: Text("Subtitulo"),
            );
          }),
    );
  }
}

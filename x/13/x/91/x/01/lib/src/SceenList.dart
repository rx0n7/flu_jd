import 'package:flutter/material.dart';

////todo
//01 - Definir um 'appbar', um 'titulo' com o texto 'Lista' e um 'Container' pequeno
////Text("Lista"),
////Edgeall 20 
//02 - Criar uma 'lista' como 'filho' para listar os elementos e inspecionar o construtor "builder"
///child: ListViewBuider
//
//05 - Dentro do 'itemBuild' passar uma funcao anonima com dois paramentros
// context, indice
 
//06 - Definir a quantide de itens no 'itemBuider' e imprimindo os indices
//itemCount: 5
//print("item: $indice");
//item: 0

class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, indice) {
            print("item: $indice");
          }),
    );
  }
}

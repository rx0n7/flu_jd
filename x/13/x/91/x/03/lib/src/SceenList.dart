import 'package:flutter/material.dart';

////todo

//01 - Definir um subtitulo na lista com um texto chamado "subtitulo"
////subititle: text ("subtitulo")

//0 
//subtitulo
//1 
//subtitulo




class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, indice) {
            return ListTile(
              title: Text(indice.toString()),
              subtitle: Text("Subtitulo")
            );
          }),
    );
  }
}

import 'package:flutter/material.dart';

////todo
//01 - Retornar um 'ListTile' para exibir os itens de uma lista e inspesionar ele
////return listTile

//02 - Definir um 'titulo' como 'text' e converter o 'indice' para uma 'string'
////title text(indice.toString)

//*Na tela do app:
//0 1 2 3 4 5

class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, indice) {
            return ListTile(
              title: Text(indice.toString())
            );
          }),
    );
  }
}

import 'package:flutter/material.dart';

////todo


//01 - Criar uma 'lista' de 'array' vazia chamada "itens" e usar um '_' underline
////List _itens = [];
///02 - Criar um metodo chamado 'carregarIntens' para carregar os itens sem retorno
////void _carregarItens
//03 - Usando um "FOR" para listar a quantidade de itens(ate 10) de 'inteiros'
////int i = 0; i <= 10; i++
//04 - Criar uma estrutura "Map" do tipo "String" e "dynamic" e depois instanciar o "Map" 
// em uma variavel chamada "item", e vamos percorrer ate  o numero '10' e para execucao do 
//'For' criamos um novo 'map'
/////Map<String, dynamic> item = Map();
//05 - Agora podemos utilizar o 'item', Criar um "titulo" e "descricao" para os indices 
//do mapa, com um texto "Lorem ipsum" e exibir o valor de "i" interpolado
//item["Titulo"] = "titulo $i Lorem ipsum dolor sit amet"
//06 - Em seguinte 'descricao' que tem a mesma estrutura, com indice 'descricao' e o valor
//item["Descricao"] = "titulo $i Lorem ipsum dolor sit amet"
////07 - Usar o '_itens.add' para adiconar os (10) itens dentro de 'item'
////_itens.add(item);
////08 - Antes de executar o 'widget' chamar o metodo de carregar os itens(depois do "build")
/////_carregarIntens();
////09 - Nao usar quantidade 5 em "itemCount" e sim colocar a quantidade de itens que temos na lista
////_itens.length
////10- Capturar o valor do "array", e imprimir o "item" na posicao "0" que eh um "Map" como "string"
////print("item " + _itens[indice].toString());
////item {titulo: titulo 10 Lorem ipsum dolor sit amet, descricao: Descricao 10 Lorem ipsum r sit amet}

//* Caso nao de imagem da um retorno no 'Scaffold'
////return Scaffold()




class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);

      //Item: {titulo: Titulo: 2 skdjfkldjf, descricao: Descricao 2 skdjfkldjf}
    }
  }

  @override
  Widget build(BuildContext context) {
    //
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            //
            print("Item: " + _itens[indice].toString());

            return ListTile(
              title: Text(indice.toString()),
              subtitle: Text("Subtitulo"),
            );
          }),
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//todo

//01 - Importar um 'conversor' para converter a resposta para um objeto 'JSON"
////import 'dart:convert';
//02 - Usando o conversor 'decode' com a 'String' que jah temos que eh  o 'response.body'
////json.decode(response.body)

//03 - Armazenando o objeto em um 'Map' com a chave 'String' e o valor 'dinamico', e
// armazenar em uma variavel chamada 'retorno' que recebe o 'json.decode'
////Map<String, dynamic> retorno = json.decode(response.body);

//04 - Recuperando os dados usando 'String' acessar as 'chaves' do mapa usando o 'retorno'
//// String logradouro = retorno['logradouro'];

//05 - Usar o um outro 'print' para fazer a exibicao de todos os dados
////print( 'Bairro: $bairro');

// Resposta: Logradouro: Praça da Sé Complemento Praça da Sé Bairro: Praça da Sé

class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  _receiverCep() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;

    response = await http.get(url);

    Map<String, dynamic> retorno = json.decode(response.body);
    String logradouro = retorno['logradouro'];
    String complemento = retorno['logradouro'];
    String bairro = retorno['logradouro'];

    print(
        'Resposta: Logradouro: $logradouro Complemento $complemento Bairro: $bairro');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico web'),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: [
            RaisedButton(
                child: Text('Clique aqui'), onPressed: () => _receiverCep())
          ],
        ),
      ),
    );
  }
}

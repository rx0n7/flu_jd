import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

//todo

//01 - Pegar a 'url' no site de buscas e criar um 'appBar' com 'titulo' e o 'body'
//"viacep.com.br/ws/01001000/json/";
//"servico de consumo de servicos web"
//02 - Em 'body' criar um 'Container' medio, com uma 'coluna' como filho, 
// e um botao como  'filhos' e adcionar o 'texto'
//edge all 40 // raiseButton // "clique aqui"
//03 - Criar um 'metodo' para recuperar o 'cep' e adcionar no 'onPress' do botao
//_receiverCep()  // onPressed: () => _receiverCep())
//04 - Configurar a 'url'  como 'string' e importar 'http' e e nomear para "http" com o 'as'
//String url = "viacep.com.br/ws/01001000/json/";
//import 'package:http/http.dart' as http;
//05 -  Utilizando  a classe 'Response'  e criamos uma variavel chamada 'response'
// que eh  do tipo 'Response' que eh uma classe que esta dentro de 'http'
//http.Response response;
//
//06 - Da um 'get' na 'url' e atribuiar a uma variavel e adcionar 'sync' nos metodos
//response = http.get(url);
//Async / await
//10 - Imprimindo a saida do 'statusCode' e do 'body' da requisicao
//print('Resposta: ' + response.statusCode.toString());
//print('Resposta: ' + response.body);

/*
  Resposta: 200
  Resposta: {
  "cep": "01001-000",
*/

class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  _receiverCep() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;

    response = await http.get(url);

    print('Resposta: ' + response.statusCode.toString());
    print('Resposta: ' + response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico web'),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: [
            RaisedButton(
                child: Text('Clique aqui'), onPressed: () => _receiverCep())
          ],
        ),
      ),
    );
  }
}

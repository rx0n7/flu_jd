import 'package:flutter/material.dart';

import 'src/ScreenFuture.dart';

void main() {
  runApp(MaterialApp(
    home: ScreenFuture(),
    debugShowCheckedModeBanner: false,
  ));
}

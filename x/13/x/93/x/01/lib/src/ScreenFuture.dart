import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

//01 -
////import 'package:http/http.dart';
//02 -
////http: ^0.12.2
///
//_receiverPrice()  {}
//03
////as http
//
///String url = 'https://blockchain.info/ticker';
//    http.Response response = await http.get(url);
//
//    return json.decode(response.body);
//
//async / await
//
//return FutureBuilder<Map>();
//
//future: ,
//

//future: _receiverPrice(),
//
//builder: (context, snapshot) {
//
//snapshot.
//
////
//
//switch(snapshot.connectionState)

//print('Conexao none');
//break;
//
//case ConnectionState.waiting:
//           print('Conexao waiting');
//break;
//
// case ConnectionState.active:
//           print('Conexao active');
//break;

//
//case ConnectionState.done:
//           print('Conexao done');
//break;

//
 //Conexao done
 

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {
  //

  Future<Map> _receiverPrice() async {
    String url = 'https://blockchain.info/ticker';

    http.Response response = await http.get(url);

    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map>(
      future: _receiverPrice(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            print('Conexao none');
            break;
          case ConnectionState.waiting:
            print('Conexao waiting');
            break;
          case ConnectionState.active:
            print('Conexao active');
            break;
          case ConnectionState.done:
            print('Conexao done');
            break;
        }
      },
    );
  }
}

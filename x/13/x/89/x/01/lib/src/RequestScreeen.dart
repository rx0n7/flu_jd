import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//todo

//01 - Para fazer a exibicao na interface dentro do 'widget' adcionar um "text"
////children: <Widget>[child, text()]

//02 - Capturar 'resultado' criamos uma 'string' chamada 'resultado' com o valor inicial 'Resultado'
////String _resultado = "Resultado";

//03 - Configurar 'resultado' dentro do 'text' e dentro do metodo 'recuperarCep' usar o "setState"
// para configurar o "_result" com os valores que jah definimos
////Text(_resultado)
//// _resultado = '$logradouro, $complemento, $bairro';


/* *Tem que aparecer na tela as seguintes informacoes:
      "cep": "01001-000",
      "logradouro": "Praça da Sé",
      "complemento": "lado ímpar",
      "bairro": "Sé",
      "localidade": "São Paulo",
      "uf": "SP",
      "ibge": "3550308",
      "gia": "1004",
      "ddd": "11",
      "siafi": "7107"
*/


class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  String _resultado = "Resultado";

  _receiverCep() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;

    response = await http.get(url);

    Map<String, dynamic> retorno = json.decode(response.body);
    String logradouro = retorno['logradouro'];
    String complemento = retorno['logradouro'];
    String bairro = retorno['logradouro'];

    setState(() {
      _resultado = '$logradouro, $complemento, $bairro';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico web'),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            Text(_resultado),
            RaisedButton(
                child: Text('Clique aqui'), onPressed: () => _receiverCep())
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//todo
//01 - Para capturar o 'cep' digitado primeiro vamos coolocar a caixa de 'text'  
//     em uma posicao que fique abaixo do botao
////Passar "_result" para abaixo do botao 'raiseButoon'
//02 - Dentro do "widget" criar um "textFild" com um teclado numerico
////keyboardType: TextInputType.number,
//03 - Criar um texto com o label: 'Digite o cep' e definir um tamanho da fonte de 20
////decoration: InputDecoration(labelText:
//// style: TextStyle(fontSize: 20),
//04 - Definir um controlador para o 'textField' e passar para o 'controller'
////TextEditingController _controllerZip = TextEditingController();
////controller: _controllerZip
//05 - Dentro do metodo criando uma 'string' e caputrar o que foi digitado como texto
//String zipTyped = _controllerZip.text;
//06- No lugar do CEP para consulta da API usamos diretamente o CEP digitado
//String url = "https://viacep.com.br/ws/$_controllerZip/json/";
//Praça da Sé, Praça da Sé, Praça da Sé


class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  //
  TextEditingController _controllerZip = TextEditingController();

  String _result = 'Resultado';

  _receiverCep() async {
    String zipTyped = _controllerZip.text;

    String url = "https://viacep.com.br/ws/$zipTyped/json/";
    http.Response response;

    response = await http.get(url);

    Map<String, dynamic> retorno = json.decode(response.body);
    String logradouro = retorno['logradouro'];
    String complemento = retorno['logradouro'];
    String bairro = retorno['logradouro'];

    setState(() {
      _result = '$logradouro, $complemento, $bairro';
      print(_result);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consumo de servico web'),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            TextField(
                keyboardType: TextInputType.number,
                decoration:
                    InputDecoration(labelText: 'Digite o cep ex: 39483948'),
                style: TextStyle(fontSize: 20),
                controller: _controllerZip),
            RaisedButton(
                child: Text('Clique aqui'), onPressed: () => _receiverCep()),
            Text(_result),
          ],
        ),
      ),
    );
  }
}

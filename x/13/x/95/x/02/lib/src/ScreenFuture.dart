import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:z_est/src/Post.dart';

import 'package:http/http.dart' as http;

//todo
//01 - Agora carrega a lista 
//Expanded(

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {
  String _urlBase = 'https://jsonplaceholder.typicode.com/';

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase + 'posts');
    var dadosJson = json.decode(response.body);

    List<Post> postings = List();
    for (var post in dadosJson) {
      print('post' + post['title']);

      Post p = Post(post['userId'], post['id'], post['title'], post['body']);
      postings.add(p);
    }

    return postings;
  }

  _post() {
    //
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Consumo de servico avancado'),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Row(
                children: [
                  RaisedButton(child: Text('Salvar'), onPressed: _post())
                ],
              ),
              Expanded(
                child: FutureBuilder<List<Post>>(
                  future: _receiverPosts(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        Center(
                          child: CircularProgressIndicator(),
                        );
                        break;
                      case ConnectionState.active:
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          print('Lista nao carregou');
                        } else {
                          print('Lista carregou');

                          return ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                //

                                List<Post> list = snapshot.data;
                                Post post = list[index];

                                return ListTile(
                                  title: Text(post.title),
                                  subtitle: Text(post.id.toString()),
                                );
                                //
                              });
                        }
                    }
                    return Container();
                  },
                ),
              )
            ],
          ),
        ));
  }
}

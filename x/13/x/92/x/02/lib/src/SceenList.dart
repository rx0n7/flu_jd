import 'package:flutter/material.dart';

//todo
//01 - Comentar ou todo o bloco do "onLongPress", e como jah temos o "indice" no 
//"listTile" entao a qualquer momento podemos saber qual item foi selecionado

//02 - Exibir qual indice foi selecioando, colocando o indice no "print"
//$indice

// Indice: 1


class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    _itens = [];
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            return ListTile(
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),

              onTap: () {
                print("Indice: $indice");

              },
            );
          }),
    );
  }
}

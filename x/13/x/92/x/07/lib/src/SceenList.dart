import 'dart:ui';

import 'package:flutter/material.dart';

//todo

//01 - Definir uma lista de "widget" do tipo "flatButton" com uma funcao anonima e um texto
////action, flatButon/raiseButton, onPressed, func anon, child, "sim",

//02 - Exibir um "print" com a frase "selecionado sim" dentro do "onPressed" e duplicar o
// flatButton(acima), colocando no texto "selecionado nao"
/*FlatButton(
                              onPressed: () {
                                print("Selecionado nao");
                              },

*/

//Selecionado sim



class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    _itens = [];
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            return ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(_itens[indice]["titulo"]),
                        titlePadding: EdgeInsets.all(20),
                        titleTextStyle:
                            TextStyle(fontSize: 20, color: Colors.orange),
                        content: Text(_itens[indice]["descricao"]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                print("Selecionado sim");
                              },
                              child: Text("Sim")),
                          FlatButton(
                              onPressed: () {
                                print("Selecionado nao");
                              },
                              child: Text("Nao"))
                        ],
                      );
                    });
              },
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),
            );
          }),
    );
  }
}

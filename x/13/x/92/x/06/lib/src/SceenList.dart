import 'package:flutter/material.dart';

//todo
//01
//titleTextStyle: TextStyle(fontSize: 20, color: Colors.orange),
//02 -
//backgroundColor: Colors.red,

class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    _itens = [];
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            return ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(_itens[indice]["titulo"]),
                        titlePadding: EdgeInsets.all(20),
                        titleTextStyle:
                            TextStyle(fontSize: 20, color: Colors.orange),
                        content: Text(_itens[indice]["descricao"]),
                        backgroundColor: Colors.red,
                      );
                    });
              },
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),
            );
          }),
    );
  }
}

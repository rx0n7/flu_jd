import 'package:flutter/material.dart';

//todo

//01 - Utilizar a funcao "showDiallog" para exibr um "widget" do tipo "dialog", 
//e em "context:" excrevemos "context", e em seguida  definir um "buider"
////showDialog, context, context

//02 - Esse "builder recebe uma funcao anonima, e essa funcao anonima retorna um 
//"alertDialog", e passamos um "context" como parametro para a funcao, e retornar 
//um "alertDialog", passamos tambem um "context" como parametro e retornamos o 
//"alertDialog", depois inspecionar o 'alertDialog'
//buider, func anonima, context, context, return alert dialog 

//03 - Primeiro vamos definir um "titulo" para o 'alertDialog'
//title, text, "titulo" //content, text, "conteudo"


class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    _itens = [];
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            return ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("Titulo"),
                        content: Text("Conteudo"),
                      );
                    });
              },
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),
            );
          }),
    );
  }
}

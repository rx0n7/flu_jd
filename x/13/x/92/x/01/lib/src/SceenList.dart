import 'package:flutter/material.dart';

//todo
//01 - Inspecionar "listTile" e usar os eventos 'onTap; e 'onLongPress' pra tratar o ckick
//// onTap + onLong + func anonima ambos

//02 - Colocar o "print" em ambas as funcoes com a frase "clique com" e ve que uma acao eh gerada
////"onPress" ou "Long press"
// Clique com onTap // Clique com longPress



class ScreenList extends StatefulWidget {
  @override
  _ScreenListState createState() => _ScreenListState();
}

class _ScreenListState extends State<ScreenList> {
  List _itens = [];

  void _loadItens() {
    _itens = [];
    for (int i = 0; i < 10; i++) {
      Map<String, dynamic> item = Map();

      item["titulo"] = "Titulo: $i skdjfkldjf";
      item["descricao"] = "Descricao $i skdjfkldjf";

      _itens.add(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    _loadItens();
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
          itemCount: _itens.length,
          itemBuilder: (context, indice) {
            return ListTile(
              title: Text(_itens[indice]["titulo"]),
              subtitle: Text(_itens[indice]["descricao"]),

              onTap: () {
                print("Clique com onTap");
              },
              onLongPress: () {
                print("Ccique longPress");
                              },
            );
          }),
    );
  }
}

import 'Post.dart';
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

//todo
/*
I/flutter ( 2238): Rsposta: 200
I/flutter ( 2238): Rsposta: {
I/flutter ( 2238):   "userId": 120,
I/flutter ( 2238):   "id": 2,
I/flutter ( 2238):   "title": "Titulo alterado",
I/flutter ( 2238):   "body": "Corpo da postagem alterado"
I/flutter ( 2238): }
*/

class ScreenFuture extends StatefulWidget {
  @override
  _ScreenFutureState createState() => _ScreenFutureState();
}

class _ScreenFutureState extends State<ScreenFuture> {
  String _urlBase = 'https://jsonplaceholder.typicode.com/';

  Future<List<Post>> _receiverPosts() async {
    http.Response response = await http.get(_urlBase + 'posts');
    var dadosJson = json.decode(response.body);

    List<Post> postings = List();
    for (var post in dadosJson) {
      print('post' + post['title']);

      Post p = Post(post['userId'], post['id'], post['title'], post['body']);
      postings.add(p);
    }

    return postings;
  }

  _post() async {
    var corpus = json.encode({
      "userId": 120,
      "id": null,
      "title": "Titulo",
      "body": "Corpo da Postagem"
    });
    http.Response response = await http.post(_urlBase + 'posts',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
        body: corpus);
    print('Rsposta: ' + response.statusCode.toString());
    print('Rsposta: ' + response.body);
  }

  _put() async {
    var corpus = json.encode({
      "userId": 120,
      "id": null,
      "title": "Titulo alterado",
      "body": "Corpo da postagem alterado"
    });
    http.Response response = await http.put(_urlBase + 'posts/2',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
        body: corpus);
    print('Rsposta: ' + response.statusCode.toString());
    print('Rsposta: ' + response.body);
  }

  _patch() {
    //
  }
  _delete() {
    //
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Consumo de servico avancado'),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Row(
                children: [
                  RaisedButton(child: Text('Salvar'), onPressed: () => _post()),
                  RaisedButton(
                      child: Text('Atualizar'), onPressed: () => _put()),
                  RaisedButton(child: Text('Apagar'), onPressed: () => _post())
                                                                                                                                                                                                                                                                                                                                                                                                TDF$P�   �5y��}�k���tB���),��y>�ZHv�^+�p`fYz��oп3��~L�yY�j���*�&s{:�8���g�: Q�|�	�怂��y'ḉҠ�}&����R�zs��]�uT5IE��n�?3�wQ��~{�J�hU:�I�j+5��g�sQ)O�Ve� Z�ǚ��$�8�B�mU�Lc�X�Ƃ���c]��W�1=|㑉����O��&4��9�Έ�LX�����ݝ��%0��u�'�؆ꈈ�,b�dp�V����R3|u'�Uxk�L�00��������b��gT��Ιm��P�jܸü<�<`N@�}�����LF��G���ۇ�`�G������������~�3��H�H��� ���Q���:ZK���6D4��IYC�MU�0���}��?-yj��S��sv��,��+���
�0W��X�f�|�,2x%�f���}i���2�W�{�ۻy��BX��z��3�!^[���kC��?yU��[q8�L����A�8�%�qj�;P����e�l ��,��ӻ�d~4?t�4�V��ˆ�v��g9)�ųWڇպѯH�8�K0�N�_�-YOr�2 0�|�Ws�x��ꚅ��FKک��H�bS���	��xKӕ�o��xG�*�?|*�y1;b�����K�e�)��U�O�T!���R�T��R���M�������j���ҙA���u��*'/���LOBE�.���)�v���3��ϡ�)�b���ꇙ0cl�*����4�l �>?l>=)�W>~�"'^���&]���.�2c�"&(6{��u�Zm����Pt����]�G��I��bj{`�j���ӣ%��ܔ�uK����;���v%�!$�_8�6���|��y�r:'b{���Xrkn�=��|2w��7Ӣ�>���!*m��/����!�w����'����u?�'�p�
r�A�>l8F��b?��_��9F��YΙ��t�繽�r��!V�����'�,؀�0�>�42��M���"x�$�M�9�.�	@Xr�܆��#��IS��{g�Z~d��t�2������J����y`��^{SG�3iTO^ ��>�v%�46�
0?��qxE^��yl�pP@����f�?y(w*���l����W��H%W��2F@�elshaU
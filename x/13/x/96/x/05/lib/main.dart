import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'src/ScreenFuture.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: ScreenFuture(),
  ));
}

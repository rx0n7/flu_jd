//06 - Criar uma classe chamada "POst"
////class Post
/////
//07 - Em  seguida definir os atributos com os seus tipos
//// _userId; _id; _title; _body;
//08 - Criar construtores nomeados para a nossa "Classe" para poder acessar os campos acima
/////Post(this._userId, this._id, this._title, this._body);
/////
//09 - Ir pra "home" tentar instanciar a classe "Post"

//12 - Como estamos usando o "underline" precisamos configurar os "get and seters"
//  String get body => _body; // set body(String value) {_body = value;}

//13 - Ir pra "home" para poder acessar os campos

//https://jsonplaceholder.typicode.com/posts

class Post {
  int _userId;
  int _id;
  String _title;
  String _body;

  Post(this._userId, this._id, this._title, this._body);

  String get body => _body;

  set body(String value) {
    _body = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  int get userId => _userId;

  set userId(int value) {
    _userId = value;
  }
}

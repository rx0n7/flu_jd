import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

//todo

//01 - Importando a bibiolteca conversor para converter a resposta em um objeto "JSON"
// convert: ^2.1.1
//02 - Decodificando de "string" para um objeto "json", a "string" jah temos(reponse.body)
//json.decode(response.boy)
//03 - Armazenar o objeto dentro de um "map" que eh basicamente "chave/valor", om uma variavel chamada "retorno" e colando o "json.decode"
//Map<String, dynamic> map = json.decode(response.body);
//04 - Recuperando dos dados, com uma "string" e o "logradouro", que recebe o retorno com a chave do "mapa" que no caso eh "logradouro" e o "complemento" com o "cep" e o "bairro"
//String logradouro = map["logradouro"];

class PlayGame extends StatefulWidget {
  @override
  _PlayGameState createState() => _PlayGameState();
}

class _PlayGameState extends State<PlayGame> {
  _receiverGames() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;

    response = await http.get(url);

    Map<String, dynamic> retorno = json.decode(response.body);

    String logradouro = retorno["logradouro"];
    String complemento = retorno["complemento"];
    String bairro = retorno["bairro"];
    String cep = retorno["cep"];

    print(
        "Resposta: Cep: $cep Logradouro: $logradouro Bairro: $bairro Complemento: $complemento");
    //Resposta: Cep: 01001-000 Logradouro: Praça da Sé Bairro: Sé Complemento: lado ímpar
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Consumo de servicoes web"),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            RaisedButton(
                child: Text("Clique nessa merda aqui"),
                onPressed: () => _receiverGames()),
          ],
        ),
      ),
    );
  }
}

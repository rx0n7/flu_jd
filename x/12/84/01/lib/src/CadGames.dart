import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

//todo
//01 - AppBar, titulo e tamanho medio do padding e texto
// edgeAll 40, "Consumir servicos web"
//02 - Criar uma coluna e dentro botao com um texto
// children, raiseButon, "clique aqui"
//03 - Criar um metodo e adcionar  no "raiseButon"
// _recCep()
//04 - Configurar "uma string" com a "url" de Cep
// https://viacep.com.br/ws/01001000/json/
//05 - Importar o "http" e nomear a biblioteca
//http: ^0.12.2 / http as http
//06 - Criar uma variavel "response" que eh do tipo "Response" que eh uma classe do "http"
//http.Response response;
//07 - Recupear os dados com "response" mais o metodo "get" que permite usar a "url" que jah temos
//response = http.get(url);
//08 - Transformar o metodo em "assyncrono" e o "http.get" em "await"
//assync / await
//09 - Imprimir o corpo da requisicao e o status code(como string)
/*response.body //response.statusCode
Resposta: 200
Resposta: {"logradouro": "Praça da Sé}
*/
//10 -


class PlayGame extends StatefulWidget {
  @override
  _PlayGameState createState() => _PlayGameState();
}

class _PlayGameState extends State<PlayGame> {
  _receiverGames() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;

    response = await http.get(url);

    print("Resposta: " + response.body);

    print("Resposta: " + response.statusCode.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Consumo de servicoes web"),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            RaisedButton(
                child: Text("Clique nessa merda aqui"),
                onPressed: () => _receiverGames()),
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//todo
//01 - Adcionar o  texto "resultado" em uma posicao melhor
// abaixo do "raiseButoon"
//02 - Adcionar um campo de texto para capturar o que o usuario digitou e adcionar os atributos
//"textField" acima do "raiseButon"
//03 - Acionar um teclado numerico
// keyboardType, textInput.Number
//
//04 - Add um label chamado "Digite um CEp"
//decoration, inputDecoration, label Text
//05 - Autmentar o tamanho da fonte pra "20"
//"textStyle"
//06 - Definir um controlador para o "textFild"
//controller: Apos o "textStyle"
//07 - Criar um controlador como atributo e passar para o "controller"
//TextEditingController _controllerZip
//08 - Capturar o cep digitado como "text" e colocar em uma string(dentro da funcao)
// String typedZip = _controllerZip.text;
//09 - Capturar diretamente o "cep" digitado
//{$typedZip}"


class ShowZip extends StatefulWidget {
  @override
  _ShowZipState createState() => _ShowZipState();
}

class _ShowZipState extends State<ShowZip> {
  String _result = "Resultado";

  TextEditingController _controllerZip = TextEditingController();

  _receiverZip() async {
    String typedZip = _controllerZip.text;
    String url = "https://viacep.com.br/ws/$typedZip/json/";

    http.Response response;
    response = await http.get(url);

    Map<String, dynamic> map = json.decode(response.body);
    String cep = map["cep"];
    String logradouro = map["logradouro"];
    String complemento = map["complemento"];
    String bairro = map["bairro"];

    setState(() {
      _result =
          ("Cep: $cep Logradouro: $logradouro Complemento: $complemento Bairro: $bairro");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Retorno na interface"),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "Digite o cep: "),
              style: TextStyle(fontSize: 20),
              controller: _controllerZip,
            ),
            RaisedButton(
                child: Text(
                  "Clique nessa merda aqui",
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () {
                  _receiverZip();
                }),
            Text(_result),
          ],
        ),
      ),
    );
  }
}

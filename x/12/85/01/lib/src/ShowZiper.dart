import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//todo
//01 - Adcionar mais um componente de interface "text" dentro do widget
//// Text(),
//02 - Capturar o "resultado" criando uma "string" chamada "resultado" com valor "resultado"
//// String _result = "Resultado";
//03 - Fazer a exibicao de resultado dentro do "Text"
///Text(_result),
//
//03 - Utiliznado o "setState" para configurar o  "_resultado" com os valores que definimos
//_result = {"Cep: $cep "};

//04 - Recuperar texto pela entrado do usuario como mumero e uma frase e formatar e um controller
//keybord: textInput / label / tamanho 20 / controller
//05 - Criar um controlador de cep e passar para controller
//editController

//06 - Capturar o cep digitado como texto dentro da funcao "reccuperarCep"
// string cepDigitado = _controllerCep
//07 - No lugar do cep da montagem da url Usar diretamente o cep digitado
// String url = "https://viacep.com.br/ws/${cepDigitado}/json/";

class ShowZip extends StatefulWidget {
  @override
  _ShowZipState createState() => _ShowZipState();
}

class _ShowZipState extends State<ShowZip> {
//02
  String _result = "Resultado";

  _receiverZip() async {
    String url = "https://viacep.com.br/ws/01001000/json/";
    http.Response response;
    response = await http.get(url);

    Map<String, dynamic> map = json.decode(response.body);
    String cep = map["cep"];
    String logradouro = map["logradouro"];
    String complemento = map["complemento"];
    String bairro = map["bairro"];

    //03
    setState(() {
      _result =
          ("Cep: $cep Logradouro: $logradouro Complemento: $complemento Bairro: $bairro");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Retorno na interface"),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          children: <Widget>[
            //01 - 03
            Text(_result),
            RaisedButton(
                child: Text(
                  "Clique nessa merda aqui",
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () {
                  _receiverZip();
                }),
          ],
        ),
      ),
    );
  }
}

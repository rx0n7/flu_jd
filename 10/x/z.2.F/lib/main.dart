import 'package:flutter/material.dart';
import 'package:z_est/src/PhraseDay.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: PhraseDay(),
  ));
}

import 'dart:math';

import 'package:flutter/material.dart';

//todo
/*
01 - criar uma frase variatica
02 - criar um array com os itens
03 - numeros randomincos
03 - funcao pegar os itens do array pelo indice do numero randomico e colcoar na sttring variatica

*/

class PhraseDay extends StatefulWidget {
  @override
  _PhraseDayState createState() => _PhraseDayState();
}

class _PhraseDayState extends State<PhraseDay> {
  //
  var _phrase = 'Clique abaixo pra gerar nova frase';

  var _itens = ['pera', 'uva', 'maca', 'salada mista'];

  void _phraseGenerated() {
    var randomNumber = Random().nextInt(_itens.length);

    setState(() {
      _phrase = _itens[randomNumber];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Frase do dia'),
        backgroundColor: Colors.green,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            border: Border.all(
          width: 3,
          color: Colors.amber,
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset('images/logo.png'),
            Text(
              _phrase,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 13,
                  color: Colors.black,
                  fontStyle: FontStyle.italic),
            ),
            RaisedButton(
                color: Colors.green,
                child: Text(
                  'Atualizar',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
                onPressed: _phraseGenerated)
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

//todo
//01 - Criar uma Barra status 'verde' e frase 'frase do  dia'  e espacamento pequeno
////Padding all 16kk
//02 - Add um 'container' com uma borda de tamanho 3 e cor amber
////decoration, box decoration, border border.all, width 3. cor amber
///
//03 - Add um 'texto' no lugar da coluna para observar o tamanho do container
////chil text?

//03 Add um widget que aceita um item abaixo do outro e a imagem como 'filhos'
//// Image.asset('images/logo.png')

//04- Add uma row no lugar da coluna para observar a diferenca entre row e column
////row

/*05 - Definir um 'texto' com a frase 'clique abaixo gerar frase', justificado, com fonte 17, italico e cor preta
Text(
              'Clique abaixo para gerar uma nova frase',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontStyle: FontStyle.italic),
            )
            */

//06 - Definir um Baotao com background, e um texto como filho,
//de cor verde com texto 'nova frase', com font 17,  e cor branca, e negrito
//raiseButton

//07 - alinhar a coluna usando o main space evelin  e cross center

//08 - ass 600 de larguro n o container

//width 600

//09 - configurar a largura com 100% do espaco disponivel,  ou Add o container dentro de center
//width double infinite

class PhraseDay extends StatefulWidget {
  @override
  _PhraseDayState createState() => _PhraseDayState();
}

class _PhraseDayState extends State<PhraseDay> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Frase do dia'),
        backgroundColor: Colors.green,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            border: Border.all(
          width: 3,
          color: Colors.amber,
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset('images/logo.png'),
            Text(
              'Clique abaixo para gerar uma nova frase',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 13,
                  color: Colors.black,
                  fontStyle: FontStyle.italic),
            ),
            RaisedButton(
                color: Colors.amber,
                child: Text(
                  'Atualizar',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
                onPressed: null)
          ],
        ),
      ),
    );
  }
}

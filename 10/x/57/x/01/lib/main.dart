import 'package:flutter/material.dart';
import 'package:z_est/src/WidgetScreen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: WidgetScreen(),
  ));
}

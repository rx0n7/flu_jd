import 'package:flutter/material.dart';

//todo

//01 - Add um titulo chamado 'Joken Po'
//// title: Text('App Joken Po'),

//02 - Add uma coluna e como filhos um espacamento em cima e em baixo
// e um texto 'escolha o app'

////Column(children: [Padding(padding: EdgeInsets.only(top: 32, bottom: 16),
////child: Text('Escolha o APP')

//03 - fortamar com fonte 20 e negrito add a imagem e duplicar o 'paddding' e mudar
//apenas o texto escolha uma opcao abaixo
//style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),

//04 - Add todas outtras tres images em linha
////Row(children: [Image.asset(''),

//05 - Alinhar a imagem pela 'linha', e editar o tamanho da altura das imagens
//crossAxisAlignment: CrossAxisAlignment.center,
// height 100

//06 - Alinhar a columna com space eveliun

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Joken Po'),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 32, bottom: 16),
            child: Text(
              'Escolha o APP',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Image.asset('images/padrao.png'),
          Padding(
            padding: EdgeInsets.only(top: 32, bottom: 16),
            child: Text(
              'Escolha uma opcao abaixo',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                'images/pedra.png',
                height: 100,
              ),
              Image.asset(
                'images/pedra.png',
                height: 100,
              ),
              Image.asset(
                'images/pedra.png',
                height: 100,
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 32),
            child: RaisedButton(
                color: Colors.blue,
                child: Text(
                  'Atualizar',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
                onPressed: null),
          )
        ],
      ),
    );
  }
}

import 'dart:math';

import 'package:flutter/material.dart';

//todo

class PharseScreen extends StatefulWidget {
  @override
  _PharseScreenState createState() => _PharseScreenState();
}

class _PharseScreenState extends State<PharseScreen> {
  //

  var _phrase = 'Clique abaixo para genrar um valor';

  var _phraseGenerated = ['pera', 'uva', 'maca', 'salada mista'];

  void _generatedPhrase() {
    var randomNumber = Random().nextInt(_phraseGenerated.length);

    setState(() {
      _phrase = _phraseGenerated[randomNumber];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Frases do dia'),
          backgroundColor: Colors.green,
        ),
        body: Center(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(width: 3, color: Colors.amber)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.only(top: 32, bottom: 12)),
                Image.asset('images/logo.png'),
                Text(
                  _phrase,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25,
                      fontStyle: FontStyle.italic,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                RaisedButton(
                    child: Text(
                      'Nova frase',
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    color: Colors.green,
                    onPressed: () {
                      _generatedPhrase();
                    })
              ],
            ),
          ),
        ));
  }
}

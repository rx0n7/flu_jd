import 'package:flutter/material.dart';
import 'package:z_est/src/JokenScreen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: JokenScreen(),
  ));
}

import 'dart:ffi';
import 'dart:math';

import 'package:flutter/material.dart';

//todo
//01 - Gerar uma opcao para o 'APP' de forma aleatoria dentro da funcao
////var options = ['pedra', 'papel', 'tesoura'];
////var number = Random().nextInt(choiceCpu.length);
////var cpuChoice = options[number];

//02 - Imprimir o resultado quando clicar na imagem
////print('Escolha da CPU: ' + cpuChoice);
////print('Escolha do Usuario: ' + userChoice);

//I/flutter ( 2186): Escolha da CPU: papel
//I/flutter ( 2186): Escolha do Usuario: pedra

class JokenScreen extends StatefulWidget {
  @override
  _JokenScreenState createState() => _JokenScreenState();
}

class _JokenScreenState extends State<JokenScreen> {
  //

  var _imgApp = AssetImage('images/padrao.png');

  void _optionSelected(String userChoice) {
    var choiceCpu = ['pedra', 'papel', 'tesoura'];
    var number = Random().nextInt(choiceCpu.length);

    var cpuChoice = choiceCpu[number];

    print('Escolha da CPU: ' + cpuChoice);

    print('Escolha do Usuario: ' + userChoice);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Joken Po'),
      ),
      body: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha do App',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Image(image: this._imgApp),
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha a opcao do usuario',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: Image.asset('images/papel.png', height: 100),
                onTap: () => _optionSelected('papel'),
              ),
              GestureDetector(
                child: Image.asset('images/pedra.png', height: 100),
                onTap: () => _optionSelected('pedra'),
              ),
              GestureDetector(
                child: Image.asset('images/tesoura.png', height: 100),
                onTap: () => _optionSelected('tesoura'),
              ),
            ],
          )
        ],
        //img
        //text result
        //'row tres img
      ),
    );
  }
}

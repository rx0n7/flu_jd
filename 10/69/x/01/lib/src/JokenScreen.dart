import 'package:flutter/material.dart';

//todo
//01 - Criar um 'Appbar' com titulo 'App Joken Po' e uma estrutura
//para colocar os items da imagem(pic) do app um embaixo do outro
////Column

//02 - Marcar a estrutura do layout do antes de criar
/*text
//img
//text result
linha 3 imagens
*/

/*03 - Criar um espacamento para colocar o 'text', com o nome 'Escolha do App' 
e formatar colocando  centralizado e mudar tamanho e estilo da fonte
padding: EdgeInsets.only(top: 32, bottom: 16),
child: Text('Escolha do App', textAlign: TextAlign.justify,
style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
*/

//04 - Definir uma imagem 'padrao' sem espacamento(pois jah configuramos acima e
//duplicar o 'padding' mudando o nome para 'Escolha a opcao do usuario',
////Image.asset('images/padrao.png'),

//05 - Adcionar tres imagens em linha reta e ajustar o tamanho das imagens
////Row(children: [Image.asset('images/papel.png'),
////height: 100,

///06 -Alinhar a coluna e o tamanho das imagens
////MainAxisAlignment.spaceEvenly,

class JokenScreen extends StatefulWidget {
  @override
  _JokenScreenState createState() => _JokenScreenState();
}

class _JokenScreenState extends State<JokenScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Joken Po'),
      ),
      body: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha do App',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Image.asset('images/padrao.png'),
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha a opcao do usuario',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset('images/papel.png', height: 100),
              Image.asset('images/papel.png', height: 100),
              Image.asset('images/tesoura.png', height: 100)
            ],
          )
        ],
        //img
        //text result
        //'row tres img
      ),
    );
  }
}

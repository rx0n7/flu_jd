import 'dart:ffi';

import 'package:flutter/material.dart';

//todo

//01 - Fazer com que a imagem do 'App' seja mudada de forma randomica
////var _imgApp = AssetImage('images/padrao.png');
////Image(image: this._imgApp),

//02 - Usar 'evento de click' para exibir com uma imagem como 'filho', mas
//criar um metodo que recebe um parametro de 'escolha do usuario e colocar
//no 'ontap' como uma funcao anonima passando o valor como 'string'
////GestureDetector, child: Image.asset('images/papel.png')
////void _optionSelected(String userChoice) {
////onTap: () => _optionSelected('pedra'),

//06 - Imprimir a opocao selecionada do usuario
//// print('Escolha do CPU: ' + userChoice);

//I/flutter ( 2897): Escolha do User: papel

class JokenScreen extends StatefulWidget {
  @override
  _JokenScreenState createState() => _JokenScreenState();
}

class _JokenScreenState extends State<JokenScreen> {
  //

  var _imgApp = AssetImage('images/padrao.png');

  void _optionSelected(String userChoice) {
    //
    print('Escolha do CPU: ' + userChoice);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Joken Po'),
      ),
      body: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha do App',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Image(image: this._imgApp),
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha a opcao do usuario',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: Image.asset('images/papel.png', height: 100),
                onTap: () => _optionSelected('papel'),
              ),
              GestureDetector(
                child: Image.asset('images/pedra.png', height: 100),
                onTap: () => _optionSelected('pedra', height: 100),
              ),
              GestureDetector(
                child: Image.asset('images/tesoura.png', height: 100),
                onTap: () => _optionSelected('tesoura'),
              ),
            ],
          )
        ],
        //img
        //text result
        //'row tres img
      ),
    );
  }
}

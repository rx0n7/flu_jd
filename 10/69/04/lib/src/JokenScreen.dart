import 'dart:math';

import 'package:flutter/material.dart';

//todo

//01 - Exibir a imagem escolhida pelo 'APP' caso seja 'pedra'
////switch (cpuChoice) {case 'pedra'://setState(() {
////this._imgApp = AssetImage('images/pedra.png');});
////break;

//02 - Transformar a mensagem estatica de baixo por uma variatica
////var _message = 'Escolha uma opcao abaixo';
////this._message,
      
/*03 -  Fazer a validacao  para o ganhador do 'CPU' e depois do usuario e empate
if ((userChoice == 'pedra' && cpuChoice == 'papel') ||
(userChoice == 'tesoura' && cpuChoice == 'pedra') ||
(userChoice == 'papel' && cpuChoice == 'tesoura')) {
//
} else if ((userChoice == 'papel' && cpuChoice == 'pedra') ||
(userChoice == 'pedra' && cpuChoice == 'tesoura') ||
(userChoice == 'tesoura' && cpuChoice == 'papel'))  {
} else {
}
*/

class JokenScreen extends StatefulWidget {
  @override
  _JokenScreenState createState() => _JokenScreenState();
}

class _JokenScreenState extends State<JokenScreen> {
  //

  var _message = 'Escolha uma opcao abaixo';

  var _imgApp = AssetImage('images/padrao.png');

  void _optionSelected(String userChoice) {
    var choiceCpu = ['pedra', 'papel', 'tesoura'];
    var number = Random().nextInt(choiceCpu.length);

    var cpuChoice = choiceCpu[number];

    print('Escolha da CPU: ' + cpuChoice);
    print('Escolha do Usuario: ' + userChoice);

    switch (cpuChoice) {
      case 'pedra':
        setState(() {
          this._imgApp = AssetImage(
            'images/pedra.png',
          );
        });
        break;
      case 'papel':
        setState(() {
          this._imgApp = AssetImage('images/papel.png');
        });
        break;
      case 'tesoura':
        setState(() {
          this._imgApp = AssetImage('images/tesoura.png');
        });
        break;
      default:
    }
    //cpu wins
    if ((userChoice == 'pedra' && cpuChoice == 'papel') ||
        (userChoice == 'tesoura' && cpuChoice == 'pedra') ||
        (userChoice == 'papel' && cpuChoice == 'tesoura')) {
      this._message = 'Voce perdeu';
    } else if ((userChoice == 'papel' && cpuChoice == 'pedra') ||
        (userChoice == 'pedra' && cpuChoice == 'tesoura') ||
        (userChoice == 'tesoura' && cpuChoice == 'papel')) {
      this._message = 'Voce ganhou';
    } else {
      //empate
      this._message = 'Empate';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Joken Po'),
      ),
      body: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                'Escolha do App',
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Image(image: this._imgApp),
          Padding(
              padding: EdgeInsets.only(top: 32, bottom: 16),
              child: Text(
                this._message,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                child: Image.asset(
                  'images/papel.png',
                  height: 100,
                ),
                onTap: () => _optionSelected('papel'),
              ),
              GestureDetector(
                child: Image.asset(
                  'images/pedra.png',
                  height: 100,
                ),
                onTap: () => _optionSelected(
                  'pedra',
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'images/tesoura.png',
                  height: 100,
                ),
                onTap: () => _optionSelected('tesoura'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
